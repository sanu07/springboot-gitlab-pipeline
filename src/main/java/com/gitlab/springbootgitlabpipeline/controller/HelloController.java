package com.gitlab.springbootgitlabpipeline.controller;

import java.time.LocalDateTime;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

	@GetMapping
	public String getValue() {
		return "Request received at " + LocalDateTime.now();
	}
}
