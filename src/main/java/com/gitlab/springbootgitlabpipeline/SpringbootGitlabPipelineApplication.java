package com.gitlab.springbootgitlabpipeline;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootGitlabPipelineApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootGitlabPipelineApplication.class, args);
	}

}
