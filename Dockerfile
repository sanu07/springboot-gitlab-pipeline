FROM openjdk:11
EXPOSE 8080
ARG JAR_FILE=target/springboot-gitlab-pipeline.jar
COPY ${JAR_FILE} springboot-gitlab-pipeline.jar
ENTRYPOINT [ "java", "-jar", "/springboot-gitlab-pipeline.jar" ]